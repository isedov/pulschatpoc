//
//  XMPPChat.h
//  pulsChat
//
//  Created by Илья Седов on 16/02/15.
//  Copyright (c) 2015 Илья Седов. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"


@interface PCXMPPChat : NSObject

@property (nonatomic, strong) NSString *server;
@property (nonatomic, assign) UInt16 port;

/**
   Подключается к заданному серверу.
   @param jid Jabber-id пользователя
   @param password Пароль для подключения
 */
- (void)connect:(NSString *)jid password:(NSString *)password;

/**
   Отправляет сообщение данному пользователю.
   @property message Текст сообщения.
   @property jid Jabber-id получателя сообщения.
 */
- (void)sendMessage:(NSString *)message to:(XMPPJID *)jid;

@property (nonatomic, copy) void (^connected)();

/// callback вызыаемый при получении сообщения
@property (nonatomic, copy) void (^gotMessage)(XMPPMessage *message);

/// callback вызываемый при получении статуса
@property (nonatomic, copy) void (^gotPresence)(XMPPPresence *presence);

/**
   Контекст хранилища ростера.
 */
- (NSManagedObjectContext *)rosterStorageContext;

/**
   Удаляет пользвателя из ростера.
   @param jid Jabber-id пользователя.
 */
- (void)deleteFromRoster:(XMPPJID *)jid;

/**
   Удаляет всех пользвателей из ростера.
    Пока эта фича похоже не работает.
 */
- (void)deleteAllFromRoster;

/**
   Выставляет статус available (Online)
 */
- (void)goOnline;

/**
   Выставлят статус available (Offline)
 */
- (void)goOffline;

@end
