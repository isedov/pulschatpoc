//
//  XMPPChat.m
//  pulsChat
//
//  Created by Илья Седов on 16/02/15.
//  Copyright (c) 2015 Илья Седов. All rights reserved.
//

#import "PCXMPPChat.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@interface PCXMPPChat () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) XMPPStream *xmppStream;
@property (nonatomic, strong) XMPPRosterCoreDataStorage *rosterStorage;
@property (nonatomic, strong) XMPPRoster *roster;

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, strong) NSMutableSet *authInProgress;

@property (nonatomic, strong) NSFetchedResultsController *fetchedController;

@end

@implementation PCXMPPChat

- (instancetype)init {
	self = [super init];
	if (self) {
		_xmppStream = [XMPPStream new];
		[_xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
		[_xmppStream setHostName:@"im.test-pulscen.ru"];
		[_xmppStream setHostPort:5002];

		_rosterStorage = [XMPPRosterCoreDataStorage new];
		_roster = [[XMPPRoster alloc] initWithRosterStorage:_rosterStorage];
		_roster.autoFetchRoster = YES;
		_roster.autoAcceptKnownPresenceSubscriptionRequests = YES;
		[_roster activate:_xmppStream];
		[_roster addDelegate:self delegateQueue:dispatch_get_main_queue()];

		_authInProgress = [NSMutableSet new];
	}
	return self;
}

- (void)connect:(NSString *)jid password:(NSString *)password {
	self.password = password ? : @"testmopc";
	NSString *myJID = [jid length] ? jid : @"test.mopc#gmail.com@im.test-pulscen.ru";
	self.login = myJID;
	self.xmppStream.myJID = [XMPPJID jidWithString:myJID];
	NSError *error;
	if ([self.xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error]) {
		DDLogVerbose(@"Connected to %@:%@", self.xmppStream.hostName, @(self.xmppStream.hostPort));
	}
	else {
		DDLogError(@"Couldn't connect to xmpp server: '%@:%@': %@",
		           self.xmppStream.hostName, @(self.xmppStream.hostPort),
		           error.localizedDescription);
	}
}

#pragma mark - Presense

- (void)goOnline {
	XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit

	//Google set their presence priority to 24, so we do the same to be compatible.
	NSXMLElement *priority = [NSXMLElement elementWithName:@"priority" stringValue:@"24"];
	[presence addChild:priority];

	[[self xmppStream] sendElement:presence];
}

- (void)goOffline {
	XMPPPresence *presence = [XMPPPresence presence];
	NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:@"away"];
	[presence addChild:show];

	[[self xmppStream] sendElement:presence];
}

- (void)sendMessage:(NSString *)message to:(XMPPJID *)jid {
	XMPPMessage *msg = [XMPPMessage messageWithType:@"chat" to:jid];
	[msg addBody:message];
	[msg addAttributeWithName:@"from" stringValue:[self.xmppStream.myJID full]];
	[self __subscribeIfNecessary:jid];

	[[self xmppStream] sendElement:msg];
}

#pragma mark - XMPPStreamDelegate

#pragma mark Connection & Authentication

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);

	NSString *expectedCertName = [self.xmppStream.myJID domain];
	if (expectedCertName) {
		settings[(NSString *)kCFStreamSSLPeerName] = expectedCertName;
	}

	settings[GCDAsyncSocketManuallyEvaluateTrust] = @(YES);
}

- (void)   xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust
    completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);

	completionHandler(YES); // пц ценит вашу заботу о безопасности!
}

- (void)xmppStreamDidSecure:(XMPPStream *)sender {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender {
	DDLogVerbose(@"XMPP did connect");

	NSError *error = nil;

	if (![self.xmppStream authenticateWithPassword:self.password error:&error]) {
		DDLogError(@"Couldn't auth XMPP: %@", error.localizedDescription);
	}
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	if (self.connected) {
		self.connected();
	}
	[self goOnline];

	NSError *error;
	[self.fetchedController performFetch:&error];
	if (error) {
		NSLog(@"Error fetching: %@", error.localizedDescription);
	}
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	DDLogError(@"Did not auth: %@", error.description);
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);

	DDLogError(@"Connection error: %@", error.localizedDescription);
}

#pragma mark Messaging & Presence

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
	DDLogInfo(@"Receive IQ: %@", iq);

//	if ([iq isSetIQ]) {
//        NSXMLElement *query = [iq childElement];
//
//	}
	return YES;
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
	DDLogVerbose(@"%@: %@ - %@", THIS_FILE, THIS_METHOD, [presence fromStr]);
	DDLogInfo(@"Presense obj: %@", presence);
	DDLogInfo(@"*** type: %@", presence.type);
	DDLogInfo(@"*** show: %@", presence.show);
	DDLogInfo(@"*** status: %@", presence.status);

	if (self.gotPresence) {
		self.gotPresence(presence);
	}
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
	DDLogInfo(@"Receive message: %@", message);
	DDLogInfo(@"=== type: %@", message.type);
	DDLogInfo(@"=== subject: %@", message.subject);
	DDLogInfo(@"=== body: %@", message.body);
	DDLogInfo(@"=== thread: %@", message.thread);
	DDLogInfo(@"=== body-for-language: %@", [message bodyForLanguage:@"en"]);
//    isChatMessage
	DDLogInfo(@"=== isChatMessage: %@", @(message.isChatMessage));
	DDLogInfo(@"=== isChatMessageWithBody: %@", @(message.isChatMessageWithBody));
	DDLogInfo(@"=== isErrorMessage: %@", @(message.isErrorMessage));
	DDLogInfo(@"=== isMessageWithBody: %@", @(message.isMessageWithBody));

	if (message.isChatMessageWithBody) {
		[self __subscribeIfNecessary:message.from];
	}

	if (self.gotMessage) {
		self.gotMessage(message);
	}
}

#pragma mark - Control sending msg & presence

- (XMPPMessage *)xmppStream:(XMPPStream *)sender willSendMessage:(XMPPMessage *)message {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	return message;
}

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender didFailToSendMessage:(XMPPMessage *)message error:(NSError *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender didSendPresence:(XMPPPresence *)presence {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender didFailToSendPresence:(XMPPPresence *)presence error:(NSError *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);

	DDLogError(@"=== error sending: %@", error);
}

#pragma mark - Roster storage

- (NSManagedObjectContext *)rosterStorageContext {
	return [self.rosterStorage mainThreadManagedObjectContext];
}

#pragma mark - Roster

- (void)__subscribeIfNecessary:(XMPPJID *)jid {
	XMPPUserCoreDataStorageObject *user = [self.rosterStorage userForJID:jid
	                                                          xmppStream:self.xmppStream
	                                                managedObjectContext:[self rosterStorageContext]];
	BOOL shouldSubscribe = NO;
	if (user) {
		if ([user.subscription isEqualToString:@"to"]) {
			[self.roster acceptPresenceSubscriptionRequestFrom:user.jid andAddToRoster:YES];
		}
		else {
			shouldSubscribe = ![user.subscription isEqualToString:@"both"];
		}
	}
	else {
		shouldSubscribe = YES;
	}

	if (shouldSubscribe) {
		[self __subscribe:jid];
	}
}

- (void)__subscribe:(XMPPJID *)jid {
	[self.roster subscribePresenceToUser:jid];
	[self.roster addUser:jid withNickname:nil];
}

- (void)xmppRoster:(XMPPRoster *)sender didReceiveBuddyRequest:(XMPPPresence *)presence {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);

	XMPPUserCoreDataStorageObject *user = [self.rosterStorage userForJID:[presence from]
	                                                          xmppStream:self.xmppStream
	                                                managedObjectContext:[self rosterStorageContext]];

	NSString *displayName = [user displayName];
	NSString *jidStrBare = [presence fromStr];
	NSString *body = nil;

	if (![displayName isEqualToString:jidStrBare]) {
		body = [NSString stringWithFormat:@"Buddy request from %@ <%@>", displayName, jidStrBare];
	}
	else {
		body = [NSString stringWithFormat:@"Buddy request from %@", displayName];
	}


	if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:displayName
		                                                    message:body
		                                                   delegate:nil
		                                          cancelButtonTitle:@"Not implemented"
		                                          otherButtonTitles:nil];
		[alertView show];
	}
	else {
		// We are not active, so use a local notification instead
		UILocalNotification *localNotification = [[UILocalNotification alloc] init];
		localNotification.alertAction = @"Not implemented";
		localNotification.alertBody = body;

		[[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
	}
}

- (NSFetchedResultsController *)fetchedController {
	if (!_fetchedController) {
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
		                                          inManagedObjectContext:self.rosterStorageContext];

		NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"sectionNum" ascending:YES];
		NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"displayName" ascending:YES];

		NSArray *sortDescriptors = @[sd1, sd2];

		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		[fetchRequest setEntity:entity];
		[fetchRequest setSortDescriptors:sortDescriptors];

		_fetchedController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
		                                                         managedObjectContext:self.rosterStorageContext
		                                                           sectionNameKeyPath:@"sectionNum"
		                                                                    cacheName:nil];
		_fetchedController.delegate = self;
	}

	return _fetchedController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	NSArray *contacts = [controller fetchedObjects];
	NSMutableString *statuses = [NSMutableString new];
	for (XMPPUserCoreDataStorageObject *user in contacts) {
		[statuses appendFormat:@"%@: %@ (%@)\n",
		 user.nickname ?      :user.jidStr,
		 user.primaryResource.show,
		 user.primaryResource.type];
	}
	NSLog(@"roster: %@", statuses);
}

- (void)deleteAllFromRoster {
	for (XMPPUserCoreDataStorageObject *user in[self.fetchedController fetchedObjects]) {
		[self deleteFromRoster:user.jid];
	}
}

- (void)deleteFromRoster:(XMPPJID *)jid {
	[self.roster unsubscribePresenceFromUser:jid];
	[self.roster removeUser:jid];
}

@end
