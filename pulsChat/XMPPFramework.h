//
//  XMPPFramework.h
//  pulsChat
//
//  Created by Илья Седов on 16/02/15.
//  Copyright (c) 2015 Илья Седов. All rights reserved.
//

#import "XMPP.h"

// Extensions

// connection watchdog
//#import "XMPPReconnect.h"

////contacts list module
#import "XMPPRoster.h"
#import "XMPPRosterCoreDataStorage.h"
