//
//  SecondViewController.m
//  pulsChat
//
//  Created by Илья Седов on 16/02/15.
//  Copyright (c) 2015 Илья Седов. All rights reserved.
//

#import "SecondViewController.h"
#import <AFNetworking.h>

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emails;
@property (weak, nonatomic) IBOutlet UITextView *output;


@property (nonatomic, strong) NSArray *checkedEmails;

@end

@implementation SecondViewController

- (void)viewDidLoad {
	[super viewDidLoad];
}

- (IBAction)__tapCheck:(id)sender {
	[self __resignIfNeeded:self.emails];

	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	self.checkedEmails = [self.emails.text componentsSeparatedByString:@","];
	NSDictionary *parameters = @{ @"emails":  self.checkedEmails };
	[manager  POST:@"http://im.test-pulscen.ru/api/v1/public/online-users"
	    parameters:parameters
	       success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    NSLog(@"JSON: (%@):%@", NSStringFromClass([responseObject class]), responseObject);
	    NSMutableString *outputStr = [NSMutableString new];
	    for (NSString * email in self.checkedEmails) {
	        [outputStr appendFormat:@"%@: %@\n", email, [responseObject[email] boolValue] ? @"online":@"offline"];
		}
	    self.output.text = outputStr;
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    NSLog(@"Error: %@", error);
	    self.output.text = error.localizedDescription;
	}];
}

- (IBAction)__tapDone:(id)sender {
	[self __resignIfNeeded:sender];
}

- (void)__resignIfNeeded:(id)responder {
	if ([responder isFirstResponder]) {
		[responder resignFirstResponder];
	}
}

@end
