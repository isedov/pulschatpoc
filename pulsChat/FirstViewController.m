//
//  FirstViewController.m
//  pulsChat
//
//  Created by Илья Седов on 16/02/15.
//  Copyright (c) 2015 Илья Седов. All rights reserved.
//

#import "FirstViewController.h"
#import "PCXMPPChat.h"

@interface FirstViewController () <NSFetchedResultsControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *output;
@property (weak, nonatomic) IBOutlet UITextField *jidInput;
@property (weak, nonatomic) IBOutlet UITextField *passwordInput;
@property (weak, nonatomic) IBOutlet UISwitch *statusToggle;

@property (nonatomic, strong) PCXMPPChat *chat;


@end

@implementation FirstViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	self.chat = [PCXMPPChat new];
	self.chat.server = @"im.test-pulscen.ru";
	self.chat.port = 5002;

	__weak typeof(self) weakSelf = self;

	self.chat.connected = ^{
		weakSelf.statusToggle.on = YES;
	};

	self.chat.gotMessage = ^(XMPPMessage *message) {
		if (message.isChatMessageWithBody) {
			[weakSelf __appendMessage:message.body];
//			[weakSelf.chat sendMessage:message.body to:message.from];
		}
	};

	self.chat.gotPresence = ^(XMPPPresence *presence) {
		weakSelf.output.text = [weakSelf.output.text stringByAppendingString:@"*****\n"];
		weakSelf.output.text = [weakSelf.output.text stringByAppendingString:[presence fromStr]];
		weakSelf.output.text = [weakSelf.output.text
		                        stringByAppendingString:[NSString stringWithFormat:@" is %@ now\n", presence.type]];
	};
}

- (IBAction)__tapConnect:(id)sender {
	[self __resignIfNeeded:self.jidInput];
	[self __resignIfNeeded:self.passwordInput];

	if ([self.passwordInput isFirstResponder]) {
		[self.passwordInput resignFirstResponder];
	}

	[self.chat connect:self.jidInput.text
	          password:self.passwordInput.text];
}

- (void)__appendMessage:(NSString *)message {
	self.output.text = [self.output.text stringByAppendingString:@"====\n"];
	self.output.text = [self.output.text stringByAppendingString:message];
	self.output.text = [self.output.text stringByAppendingString:@"\n"];
}

- (IBAction)__dismissKeyboard:(id)sender {
	[sender resignFirstResponder];
}

- (void)__resignIfNeeded:(id)responder {
	if ([responder isFirstResponder]) {
		[responder resignFirstResponder];
	}
}

- (IBAction)__tapToggleStatus:(id)sender {
	UISwitch *toggle = (UISwitch *)sender;
	if (toggle.on) {
		[self.chat goOnline];
	}
	else {
		[self.chat goOffline];
	}
}

- (IBAction)__delRoster:(id)sender {
	[self.chat deleteAllFromRoster];
}

- (IBAction)__sendMsg:(id)sender {
	UIAlertView *askAlert = [[UIAlertView alloc] initWithTitle:@"Send message"
	                                                   message:@"Enter jid of user:"
	                                                  delegate:self
	                                         cancelButtonTitle:@"Cancel"
	                                         otherButtonTitles:@"OK", nil];
	askAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
	[askAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex != alertView.cancelButtonIndex) {
		UITextField *jidInput = [alertView textFieldAtIndex:0];
		XMPPJID *toJID = [XMPPJID jidWithString:jidInput.text];
		[self.chat sendMessage:@"test message" to:toJID];
	}
}

@end
