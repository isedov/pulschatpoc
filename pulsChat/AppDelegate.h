//
//  AppDelegate.h
//  pulsChat
//
//  Created by Илья Седов on 16/02/15.
//  Copyright (c) 2015 Илья Седов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

